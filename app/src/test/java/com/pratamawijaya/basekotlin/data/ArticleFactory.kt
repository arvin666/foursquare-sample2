package com.pratamawijaya.basekotlin.data

import com.pratamawijaya.basekotlin.DataFactory
import com.pratamawijaya.basekotlin.data.model.SongModel
import com.pratamawijaya.basekotlin.domain.Song

object ArticleFactory {
    fun makeArticleModel(): SongModel {
        return SongModel(
                title = DataFactory.randomString(),
                url = DataFactory.randomString(),
                urlToImage = DataFactory.randomString(),
                content = DataFactory.randomString(),
                author = DataFactory.randomString()
        )
    }

    fun makeArticleDomain(): Song {
        return Song(
                title = DataFactory.randomString(),
                url = DataFactory.randomString(),
                imageUrl = DataFactory.randomString(),
                content = DataFactory.randomString(),
                author = DataFactory.randomString()
        )
    }

    fun makeListArticleModel(): List<SongModel> {
        return listOf(
                makeArticleModel(),
                makeArticleModel(),
                makeArticleModel()
        )
    }

    fun makeListArticleDomain(): List<Song>{
        return listOf(
                makeArticleDomain(),
                makeArticleDomain(),
                makeArticleDomain()
        )
    }
}