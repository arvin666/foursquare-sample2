package com.pratamawijaya.basekotlin.data.mapper

import com.pratamawijaya.basekotlin.data.ArticleFactory
import com.pratamawijaya.basekotlin.data.model.SongModel
import com.pratamawijaya.basekotlin.domain.Song
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class SongMapperTest {

    private lateinit var articleMapper: ArticleMapper

    @Before
    fun setUp() {
        articleMapper = ArticleMapper()
    }

    @Test
    fun `test map article model to domain`() {
        val articleModel = ArticleFactory.makeArticleModel()
        val articleDomain = articleMapper.mapToDomain(articleModel)

        assertEqualsData(articleModel, articleDomain)
    }


    @Test
    fun `test map article domain to model`() {
        val articleDomain = ArticleFactory.makeArticleDomain()
        val articleModel = articleMapper.mapToModel(articleDomain)

        assertEqualsData(articleModel, articleDomain)
    }

    private fun assertEqualsData(songModel: SongModel, songDomain: Song) {
        assertEquals(songModel.title, songDomain.title)
        assertEquals(songModel.author, songDomain.author)
        assertEquals(songModel.content, songDomain.content)
        assertEquals(songModel.url, songDomain.url)
        assertEquals(songModel.urlToImage, songDomain.imageUrl)
    }
}