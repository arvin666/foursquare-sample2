package com.pratamawijaya.basekotlin.data.repository

import com.pratamawijaya.basekotlin.data.response.explore.ExploreResponse
import io.reactivex.Single

interface PlacesReposetory {
    fun getExplore(offset:String,ll:String): Single<ExploreResponse>
}