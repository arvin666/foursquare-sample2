//package com.pratamawijaya.basekotlin.data.mapper
//
//import com.pratamawijaya.basekotlin.data.database.entity.PlaceEntity
//import com.pratamawijaya.basekotlin.data.model.SongModel
//import com.pratamawijaya.basekotlin.domain.Song
//
//open class ArticleMapper : BaseMapper<SongModel, Song> {
//
//    override fun mapToDomain(model: SongModel): Song {
//        return Song(
//                title = model.title,
//                author = model.author ?: "",
//                content = model.content ?: "",
//                url = model.url,
//                imageUrl = model.urlToImage ?: ""
//        )
//    }
//
//    fun mapToEntity(model: SongModel): PlaceEntity {
//        return PlaceEntity(
//                url = model.url,
//                title = model.title,
//                author = model.author ?: "",
//                content = model.content ?: "",
//                urlImage = model.urlToImage ?: ""
//        )
//    }
//
//    fun mapToListEntity(models: List<SongModel>): List<PlaceEntity> {
//        var listEntity = mutableListOf<PlaceEntity>()
//
//        models.map {
//            listEntity.add(mapToEntity(it))
//        }
//
//        return listEntity
//    }
//
//    override fun mapToModel(domain: Song): SongModel {
//        return SongModel(
//                title = domain.title,
//                url = domain.url,
//                content = domain.content,
//                author = domain.author,
//                urlToImage = domain.imageUrl
//        )
//    }
//}