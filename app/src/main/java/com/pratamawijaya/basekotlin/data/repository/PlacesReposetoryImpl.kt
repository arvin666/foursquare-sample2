package com.pratamawijaya.basekotlin.data.repository


import com.pratamawijaya.basekotlin.data.database.dao.PlacesDao
import com.pratamawijaya.basekotlin.data.response.explore.ExploreResponse
import com.pratamawijaya.basekotlin.data.services.PlacesService
import io.reactivex.Single

open class PlacesReposetoryImpl(private val service: PlacesService,
                                private val placesDao: PlacesDao) : PlacesReposetory {
    override fun getExplore(offset: String,ll:String): Single<ExploreResponse> {
         return service.explorePlaces(ll,offset)
    }



}