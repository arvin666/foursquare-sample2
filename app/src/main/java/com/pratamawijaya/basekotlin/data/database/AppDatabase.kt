package com.pratamawijaya.basekotlin.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.pratamawijaya.basekotlin.data.database.dao.PlacesDao
import com.pratamawijaya.basekotlin.data.database.entity.PlaceEntity

@Database(entities = [PlaceEntity::class], version = 1)
public abstract class AppDatabase : RoomDatabase() {
    abstract fun placesDao(): PlacesDao
}