package com.pratamawijaya.basekotlin.data.response

import com.pratamawijaya.basekotlin.data.model.SongModel

data class TopHeadlineResponse(val status: String, val songs: List<SongModel>)