package com.pratamawijaya.basekotlin.data.services

import com.pratamawijaya.basekotlin.data.response.TopHeadlineResponse
import com.pratamawijaya.basekotlin.data.response.explore.ExploreResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PlacesService {

    companion object {
        private const val CLIENT_ID = "J4LTSK0DINJKL3X4MCE2ESH30DZVO11N4FVEXWSPQLOSGZVE"
        private const val CLIENT_SECRET = "P2CP0HDKS0UD1YFAPVWH3VFTOP2ZJT1QE0RRLXIL3GP5DMOK"
        private const val VERSION = "20180323"
        private const val COMMON_PARAMS = "&client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&v=$VERSION&radius=30000"

        //47.606200,-122.332100     ll=47.606200,-122.332100&
    }

    @GET("venues/explore?limit=20$COMMON_PARAMS")
    fun explorePlaces(@Query("ll") latlng: String,@Query("offset") offset: String): Single<ExploreResponse>

    @GET("venues/{venue_id}/?$COMMON_PARAMS")
    fun getDetails(@Path("venue_id") venueId: String): Single<String>

}