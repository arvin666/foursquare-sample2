package com.pratamawijaya.basekotlin.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.pratamawijaya.basekotlin.data.database.entity.PlaceEntity
import io.reactivex.Single

@Dao
interface PlacesDao{

    @Query("SELECT * from places")
    fun getPlaces() : Single<List<PlaceEntity>>

    @Insert
    fun saveAllArticles(articles:List<PlaceEntity>)
}