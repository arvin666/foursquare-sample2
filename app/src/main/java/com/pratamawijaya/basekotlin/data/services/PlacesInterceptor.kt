package com.pratamawijaya.basekotlin.data.services

import com.pratamawijaya.basekotlin.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class PlacesInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder()
                .addHeader("test", "test")
                .build()
        return chain.proceed(request)
    }
}