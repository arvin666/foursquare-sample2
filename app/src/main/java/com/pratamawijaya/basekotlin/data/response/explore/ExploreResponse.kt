package com.pratamawijaya.basekotlin.data.response.explore
import com.google.gson.annotations.SerializedName

data class ExploreResponse (

	@SerializedName("meta") val meta : Meta,
	@SerializedName("response") val response : Response
)