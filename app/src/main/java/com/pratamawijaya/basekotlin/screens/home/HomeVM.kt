package com.pratamawijaya.basekotlin.screens.home

import androidx.lifecycle.MutableLiveData
import com.github.ajalt.timberkt.d
import com.github.ajalt.timberkt.e
import com.pratamawijaya.basekotlin.data.repository.PlacesReposetory
import com.pratamawijaya.basekotlin.data.response.explore.Venue
import com.pratamawijaya.basekotlin.screens.base.BaseViewModel
import com.pratamawijaya.basekotlin.shared.RxUtils

sealed class HomeScreenState
object LoadingState : HomeScreenState()
data class ErrorState(var msg: String?) : HomeScreenState()
data class VenueLoadedState(val venues: List<Venue>) : HomeScreenState()

class HomeVM(val repo: PlacesReposetory) : BaseViewModel() {

    var homeState = MutableLiveData<HomeScreenState>()
    var venueList = mutableListOf<Venue>()



    fun getExplore(query: String, page: Int) {
        homeState.value = LoadingState
        val offset = ((page-1)*20).toString()
        compositeDisposable.add(
                repo.getExplore(offset = offset,  ll = "40.7128,-74.0060")
                        .compose(RxUtils.applySingleAsync())
                        .subscribe({ result ->
                            var newVenues = mutableListOf<Venue>()

                            result.response.groups.forEach {
                                it.items.forEach { newVenues.add(it.venue) }
                            }
                            venueList.addAll(newVenues)
                            homeState.value = VenueLoadedState(newVenues)

                            d { "venue size ${venueList.size}" }
                        }, this::onError))
    }

    override fun onError(error: Throwable) {
        e { "error ${error.localizedMessage}" }
        homeState.value = ErrorState(error.localizedMessage)
    }
}