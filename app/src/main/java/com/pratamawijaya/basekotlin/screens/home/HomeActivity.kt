package com.pratamawijaya.basekotlin.screens.home

import android.Manifest
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.ajalt.timberkt.d
import com.pratamawijaya.basekotlin.R
import com.pratamawijaya.basekotlin.shared.PaginationScrollListener
import com.pratamawijaya.basekotlin.shared.view.LoadmoreItemView
import com.pratamawijaya.basekotlin.shared.view.VenueItemView
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.activity_home.*
import org.koin.android.ext.android.inject

class HomeActivity : AppCompatActivity() {

    private val homeAdapter = GroupAdapter<ViewHolder>()

    private val vm: HomeVM by inject()

    private var page = 1
    private var isLoadMore = false
    private var isLastPage = false

    private var loadmoreItemView = LoadmoreItemView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(my_toolbar)

        val linearLayoutManager = LinearLayoutManager(this)

        requestPermission()

        rvHome.apply {
            layoutManager = linearLayoutManager
            adapter = homeAdapter
        }

        var query = "iran"

        vm.homeState.observe(this, stateObserver)
        vm.getExplore(query = query, page = page)

        rvHome.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return this@HomeActivity.isLoadMore
            }

            override fun loadMoreItems() {
                isLoadMore = true
                page++

                d { "loadmore page $page" }

                vm.getExplore(query, page)
            }
        })
    }

    private fun requestPermission() {
        val permisions= arrayOf<String>(Manifest.permission.READ_EXTERNAL_STORAGE)

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
//            requestPermissions(permisions,100)
//           // else resolveMusics()
//        }


    }

    private val stateObserver = Observer<HomeScreenState> { state ->
        when (state) {
            is LoadingState -> {
                if (isLoadMore) {
                    // add loading indicator
                    homeAdapter.add(loadmoreItemView)
                } else {
                    // todo: setup loading state
                }
            }
            is ErrorState -> {
            }
            is VenueLoadedState -> {
                if (isLoadMore) {
                    // remove loading indicator
                    homeAdapter.remove(loadmoreItemView)
                    isLoadMore = false
                }

                state.venues.map {
                    homeAdapter.add(VenueItemView(it))
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode==555&&grantResults.isNotEmpty())
        {
        }
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        return true
    }

}
