package com.pratamawijaya.basekotlin.shared.view

import com.pratamawijaya.basekotlin.R
import com.pratamawijaya.basekotlin.data.response.explore.Venue
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.layout_item_article.view.*

class VenueItemView(private val venue: Venue) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        //val imgArticle = viewHolder.itemView.imgArticle
        val titleVenue = viewHolder.itemView.title
        val addressVenue = viewHolder.itemView.address

       // imgArticle.loadSrc(song.imageUrl)
        titleVenue.text = venue.name
        addressVenue.text = venue.location.address
    }



    override fun getLayout(): Int = R.layout.layout_item_article
}